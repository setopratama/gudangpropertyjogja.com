  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Info!</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Kami meminta maaf atas ketidaknyamanan ini, tetapi saat ini situs web kami sedang menjalani proses perbaikan dan pengembangan untuk memberikan pengalaman pengguna yang lebih baik. Kami sangat menghargai kunjungan Anda, dan kami bekerja keras untuk memastikan bahwa setiap aspek situs kami akan lebih baik dari sebelumnya. Meskipun saat ini situs web kami tidak tersedia, harap tunggu sebentar, karena kami berusaha secepat mungkin untuk menghadirkan konten yang lebih menarik dan fitur-fitur baru yang akan memberikan manfaat yang lebih besar. Terima kasih atas pengertian dan kesabaran Anda. Kami akan segera kembali online, dan Anda akan segera dapat menikmati hasil dari perbaikan kami.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
      </div>
    </div>
  </div>